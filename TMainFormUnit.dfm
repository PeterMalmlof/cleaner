object MainForm: TMainForm
  Left = 458
  Top = 660
  Width = 373
  Height = 351
  Caption = 'MainForm'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PopupMenu = AppMenu
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  OnResize = FormResize
  PixelsPerInch = 96
  TextHeight = 13
  object CountLab: TLabel
    Left = 240
    Top = 296
    Width = 46
    Height = 13
    Caption = 'CountLab'
  end
  object BtnExit: TButton
    Left = 8
    Top = 288
    Width = 49
    Height = 25
    Caption = 'Exit'
    TabOrder = 0
    OnClick = BtnExitClick
  end
  object BtnScan: TButton
    Left = 64
    Top = 288
    Width = 49
    Height = 25
    Caption = 'Scan'
    TabOrder = 1
    OnClick = BtnScanClick
  end
  object BtnClean: TButton
    Left = 176
    Top = 288
    Width = 49
    Height = 25
    Caption = 'Clean'
    TabOrder = 2
    OnClick = BtnCleanClick
  end
  object Panel: TPanel
    Left = 8
    Top = 40
    Width = 353
    Height = 217
    TabOrder = 3
    object Splitter1: TSplitter
      Left = 1
      Top = 42
      Width = 351
      Height = 7
      Cursor = crVSplit
      Align = alTop
      OnMoved = Splitter1Moved
    end
    object Splitter2: TSplitter
      Left = 1
      Top = 90
      Width = 351
      Height = 7
      Cursor = crVSplit
      Align = alTop
      OnMoved = Splitter2Moved
    end
    object FolderListView: TListView
      Left = 1
      Top = 1
      Width = 351
      Height = 41
      Align = alTop
      Columns = <
        item
          Caption = 'Files'
        end
        item
          Caption = 'Folders'
        end>
      HideSelection = False
      RowSelect = True
      ShowColumnHeaders = False
      TabOrder = 0
      ViewStyle = vsReport
      OnDblClick = FolderListViewDblClick
      OnEdited = FolderListViewEdited
    end
    object FileListView: TListView
      Left = 1
      Top = 49
      Width = 351
      Height = 41
      Align = alTop
      Columns = <
        item
          Caption = 'Files'
        end>
      HideSelection = False
      ReadOnly = True
      RowSelect = True
      ShowColumnHeaders = False
      TabOrder = 1
      ViewStyle = vsReport
    end
    object LogWin: TMemo
      Left = 1
      Top = 97
      Width = 351
      Height = 119
      Align = alClient
      Lines.Strings = (
        'LogWin')
      ReadOnly = True
      ScrollBars = ssVertical
      TabOrder = 2
      WantReturns = False
      WantTabs = True
      WordWrap = False
    end
  end
  object BtnClear: TButton
    Left = 120
    Top = 288
    Width = 49
    Height = 25
    Caption = 'Clear'
    TabOrder = 4
    OnClick = BtnClearClick
  end
  object AppProp1: TAppProp
    pHintOn = True
    pBorder = True
    pFont.Charset = DEFAULT_CHARSET
    pFont.Color = clBlack
    pFont.Height = -12
    pFont.Name = 'Verdana'
    pFont.Style = []
    pBackColor = clBtnFace
    pForeColor = clSilver
    pHighColor = clCream
    DblBuff = True
    OnLog = Log
    Left = 8
    Top = 8
  end
  object PmaLog1: TPmaLog
    OnLog = LogToWin
    Left = 48
    Top = 8
  end
  object AppMenu: TGenPopupMenu
    AutoHotKeys = maManual
    OwnerDraw = True
    OnPopup = AppMenuPopup
    OnLog = Log
    BackColor = clBtnFace
    ForeColor = clGray
    HighColor = clCream
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlack
    Font.Height = 12
    Font.Name = 'Ariel'
    Font.Style = []
    WindowHandle = 1376856
    Left = 80
    Top = 8
  end
end
