unit TMainFormUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, Menus,    ComCtrls, ShellApi,Contnrs,

  TPmaLogUnit,
  TGenPopupMenuUnit,
  TGenAppPropUnit, ExtCtrls;

type
  TMainForm = class(TForm)
    AppProp1: TAppProp;
    PmaLog1: TPmaLog;
    AppMenu: TGenPopupMenu;
    BtnExit: TButton;
    BtnScan: TButton;
    BtnClean: TButton;
    CountLab: TLabel;
    Panel: TPanel;
    FolderListView: TListView;
    FileListView: TListView;
    LogWin: TMemo;
    Splitter1: TSplitter;
    Splitter2: TSplitter;
    BtnClear: TButton;
    procedure LogToWin(sLine: String);
    procedure FormResize(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure AppMenuPopup(Sender: TObject);
    procedure FolderListViewDblClick(Sender: TObject);
    procedure BtnExitClick(Sender: TObject);
    procedure BtnScanClick(Sender: TObject);
    procedure BtnCleanClick(Sender: TObject);
    procedure Splitter1Moved(Sender: TObject);
    procedure Splitter2Moved(Sender: TObject);
    procedure BtnClearClick(Sender: TObject);
    procedure Log(sLine: String);
    procedure FolderListViewEdited(Sender: TObject; Item: TListItem;
      var S: String);
  private
    FRunning        : boolean;
    FFolderPropList : TGenAppPropStringList;
    FTotSize        : int64;
    FScanned : boolean;

    FSlitterA : TGenAppPropInt;
    FSlitterB : TGenAppPropInt;
    FTimer    : TTimer;
  protected

    procedure LoadFolders;
    procedure SaveFolders;
    procedure ScanFolders;
    procedure ScanCurFolder;
    procedure ClearFolders;

    procedure CleanFiles;
    procedure ClearFiles;

    procedure UiRefresh;

    procedure OnScanSelected  (Sender : TObject);
    procedure OnMyExit  (Sender : TObject);
    procedure OnMyTimer (Sender : TObject);

  public
    { Public declarations }
  end;

var
  MainForm: TMainForm;

implementation

{$R *.dfm}

uses
  TGenStrUnit,
  TPmaFormUtils,
  TGenFileSystemUnit,
  TGenGraphicsUnit;

const
  prfPref       = ' Preferences';
  prfFolders    = 'Folders';
  prfLogVisible = 'LogVisible';
  prfSplitterA  = 'SplitterA';
  prfSplitterB  = 'SplitterB';

  BRD     = 2;
  BTNHGT  = 24;
  BTNWDT  = 48;
  FILEWDT = 50;

//------------------------------------------------------------------------------
//
//                                 CREATE & DESTROY
//
//------------------------------------------------------------------------------
//  Create Form
//------------------------------------------------------------------------------
procedure TMainForm.FormCreate(Sender: TObject);
begin
  FRunning := false;

  Application.Title := 'Cleaner';
  self.Caption      := 'Cleaner';
  CountLab.Caption  := 'No Files';
  FTotSize := 0;

  App.StartUp;
  AppMenu.StartUp;

  SetDblBuffered(self, true);

  // Load All Folders to Clean

  FFolderPropList := App.CreatePropStringList(prfFolders);

  LoadFolders;

  // Set Up Ui

  FSlitterA := App.CreatePropInt(prfPref, prfSplitterA, 100);
  FSlitterB := App.CreatePropInt(prfPref, prfSplitterB, 100);

  FolderListView.Height := FSlitterA.pInt;
  FileListView.Height   := FSlitterB.pInt;

  FTimer := TTimer.Create(nil);
  FTimer.Interval := 100;
  FTimer.OnTimer  := OnMyTimer;
  FTimer.Enabled  := true;

  FScanned := false;
  FRunning := true;

  Log('Opened');
  Log(StringOfChar('-',80));
end;
//------------------------------------------------------------------------------
//  Refresh Ui and Columns after Creating
//------------------------------------------------------------------------------
procedure TMainForm.OnMyTimer(Sender : TObject);
begin
  FTimer.Enabled := false;

  UiRefresh;
  //ScanFolders;
end;
//------------------------------------------------------------------------------
//  Destroy Form
//------------------------------------------------------------------------------
procedure TMainForm.FormDestroy(Sender: TObject);
begin
  FRunning := false;

  Log(StringOfChar('-',80));
  Log('Destroy');

  // Save Folders

  SaveFolders;

  // Remove all Folders

  ClearFolders;

  Log('Fole Count ' + IntToStr(TGenFileSystem.GetDebugCount));
  App.ShutDown;
  AppMenu.ShutDown;
end;
//------------------------------------------------------------------------------
//
//                                     UI
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.UiRefresh;
begin
  FolderListView.Columns[0].Width := FILEWDT;
  FolderListView.Columns[1].Width := FolderListView.ClientWidth - FILEWDT;
  
  FileListView.Columns[0].Width   := FileListView.ClientWidth;

  if (FileListView.Items.Count = 0) then
    CountLab.Caption  := 'No Files'
  else
    CountLab.Caption  := 'Files: ' + IntToStr(FileListView.Items.Count) +
                         ' Size: ' + SizeToStr(FTotSize);


  BtnClean.Enabled := (FileListView.Items.Count > 0);
  BtnClear.Enabled := (FileListView.Items.Count > 0);
end;
//------------------------------------------------------------------------------
//
//                                    FOLDER LIST
//
//------------------------------------------------------------------------------
//  Load All Folders from Ini File to be cleaned
//------------------------------------------------------------------------------
procedure TMainForm.LoadFolders;
var
  Iter     : integer;     // Iterator
  Name     : string;      // Folder Name
  Value    : string;      // Wild Card
  pFolder  : TGenFolder;  // Created TGenFolder
  ListItem : TListItem;   // List Item to Add
begin
  // Clear all Folders

  FolderListView.Items.Clear;

  // Load All Folders to Clean

  Iter := 0;
  while FFolderPropList.GetNext(Iter, Name, Value) do
    begin
      Log('Folder Added ' + Name + ' Wild ' + Value);

      // Create a TGenFolder

      pFolder := TGenFolder.CreateWild(nil, Name, Value);

      ListItem := FolderListView.Items.Add;
      ListItem.Caption := Value;
      ListItem.SubItems.Add(Name);
      ListItem.Data := pFolder;
    end;

end;
//------------------------------------------------------------------------------
//  Save All Folders to Ini File
//------------------------------------------------------------------------------
procedure TMainForm.SaveFolders;
var
  Ind : integer;
begin

  // Clear Old Values

  FFolderPropList.Clear;

  // Add ListItems from FolderListView

  if FolderListView.Items.Count > 0 then
    for Ind := 0 to FolderListView.Items.Count - 1 do
      begin
        FFolderPropList.AddNameValue(
          TGenFolder(FolderListView.Items[Ind].Data).pPathName,
          TGenFolder(FolderListView.Items[Ind].Data).pWild);
      end;

end;
//------------------------------------------------------------------------------
//  Scan for all Files in all Folders
//------------------------------------------------------------------------------
procedure TMainForm.ScanFolders;
var
  ListItem  : TListItem;
  pFolder   : TGenFolder;
  Iter, Ind : integer;
  pFole     : TGenFole;
begin
  // Clear all Old Files Found

  ClearFiles;
  FTotSize := 0;

  if (FolderListView.Items.Count > 0) then
    for Ind := 0 to FolderListView.Items.Count - 1 do
      begin
        ListItem := FolderListView.Items[Ind];
        if Assigned(ListItem) then
          begin

            pFolder := ListItem.Data;

            pFolder.RefreshWild;

            Iter := 0;
            while pFolder.IterFoles(Iter, pFole) do
              begin
                ListItem := FileListView.Items.Add;
                ListItem.Caption := pFole.pPathName;
                ListItem.Data    := pFole;

                FTotSize := FTotSize + pFole.pSize;
              end;
          end;
      end;

  UiRefresh;

  FScanned := true;
end;
//------------------------------------------------------------------------------
//  Scan for all Files in all Folders
//------------------------------------------------------------------------------
procedure TMainForm.ScanCurFolder;
var
  ListItem  : TListItem;
  pFolder   : TGenFolder;
  Iter      : integer;
  pFole     : TGenFole;
begin
  // Clear all Old Files Found

  ClearFiles;
  FTotSize := 0;

  ListItem := FolderListView.Selected;
   if Assigned(ListItem) then
    begin

      pFolder := ListItem.Data;

      pFolder.RefreshWild;

      Iter := 0;
      while pFolder.IterFoles(Iter, pFole) do
        begin
          ListItem := FileListView.Items.Add;
          ListItem.Caption := pFole.pPathName;
          ListItem.Data    := pFole;

          FTotSize := FTotSize + pFole.pSize;
        end;
    end;

  UiRefresh;

  FScanned := true;
end;
//------------------------------------------------------------------------------
//  Clear All Folders
//------------------------------------------------------------------------------
procedure TMainForm.ClearFolders;
var
  Ind : integer;
begin

  ClearFiles;

  if FolderListView.Items.Count > 0 then
    for Ind := 0 to FolderListView.Items.Count -1 do
      TGenFolder(FolderListView.Items[Ind]).Free;

  FolderListView.Items.Clear;

  FScanned := false;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.FolderListViewDblClick(Sender: TObject);
var
  Pos       : TPoint;
  pFolder   : TGenFolder;
  NewFolder : string;
  ListItem  : TListItem;
begin
  // Is There a ListItem underneath

  Pos := FolderLIstView.ScreenToClient(Mouse.CursorPos);
  ListItem := FolderLIstView.GetItemAt(Pos.X, Pos.Y);
  if Assigned(ListItem) then
    begin
      pFolder := ListItem.Data;

      // Its an already loaded folder

      if pFolder.Pick(pFolder.pPathName, 'Pick Folder to Clean', NewFolder) then
        begin
          pFolder.pFileName := NewFolder;

          ListItem.SubItems[0] := pFolder.pPathName;

          pFolder.RefreshWild;

          Log('Folder Changed ' + NewFolder);

          if FScanned then ScanFolders;
        end;
    end
  else
    begin
      // Its a New Folder

      if TGenFolder.Pick('', 'Pick Folder to Clean', NewFolder) then
        begin
          if (not FFolderPropList.IsName(NewFolder)) then
            begin
              // Create a New Folder

              pFolder := TGenFolder.CreateWild(nil, NewFolder,'*.Log');

              FFolderPropList.AddNameValue(pFolder.pPathName,pFolder.pWild);

              ListItem := FolderListView.Items.Add;
              ListItem.Caption := pFolder.pWild;
              ListItem.SubItems.Add(pFolder.pPathName);
              ListItem.Data := pFolder;

              Log('Folder Added ' + NewFolder);

              if FScanned then ScanFolders;
            end;
        end;
    end;
end;
//------------------------------------------------------------------------------
//  An Item has maybe changed Caprion
//------------------------------------------------------------------------------
procedure TMainForm.FolderListViewEdited(Sender: TObject; Item: TListItem;
  var S: String);
var
  pFole : TGenFolder;
begin
  Log('Wild: ' + S);

  pFole := Item.Data;
  if Assigned(pFole) and (pFole is TGenFolder) then
    begin
      pFole.pWild := S;

      if FScanned then ScanFolders;
    end;
end;

//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.BtnScanClick(Sender: TObject);
begin
  ScanFolders;
end;
//------------------------------------------------------------------------------
//
//                                     FILE LIST
//
//------------------------------------------------------------------------------
//  Clear all Foles in File List and Free all TGenFoles
//------------------------------------------------------------------------------
procedure TMainForm.ClearFiles;
var
  Ind : integer;
begin
  if FileListView.Items.Count > 0 then
    for Ind := 0 to FileListView.Items.Count -1 do
      TGenFole(FileListView.Items[Ind]).Free;

  FileListView.Items.Clear;

  UiRefresh;
  FScanned := false;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.CleanFiles;
var
  Ind   : integer;
  pFole : TGenFole;
begin
  Log('Cleaning');

  // Clean all Files in FileList

  if (FileListView.Items.Count > 0) then
    begin
      for Ind := 0 to FileListView.Items.Count - 1 do
        begin
          pFole := FileListView.Items[Ind].Data;
          Log(pFole.pPathName);
          pFole.Delete;
        end;
    end;
  FScanned := false;

  // Rescan all

  ScanFolders;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.BtnCleanClick(Sender: TObject);
begin
  CleanFiles;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.BtnClearClick(Sender: TObject);
begin
  ClearFiles;
end;
//------------------------------------------------------------------------------
//
//                                     APP MENU
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.AppMenuPopup(Sender: TObject);
var
  pPref : TGenMenuITem;
begin

  AppMenu.Items.Clear;
  AppMenu.Font      := App.pFont;
  AppMenu.BackColor := App.pBackColor;
  AppMenu.ForeColor := App.pForeColor;
  AppMenu.HighColor := App.pHighColor;

  pPref := TGenMenuItem.Create(AppMenu);
  pPref.Caption := 'Preferemces ';
  AppMenu.Items.Add(pPref);

  App.AddPrefMenu(AppMenu, pPref , false, false);

  if Assigned(FolderlistView.Selected) then
    begin
      pPref := TGenMenuItem.Create(AppMenu);
      pPref.Caption := 'Scan Selected';
      pPref.OnClick := OnScanSelected;
      AppMenu.Items.Add(pPref);
    end;

  pPref := TGenMenuItem.Create(AppMenu);
  pPref.Caption := '-';
  AppMenu.Items.Add(pPref);

  pPref := TGenMenuItem.Create(AppMenu);
  pPref.Caption := 'Exit';
  pPref.OnClick := OnMyExit;
  AppMenu.Items.Add(pPref);

end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.OnMyExit(Sender : TObject);
begin
  self.close;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.BtnExitClick(Sender: TObject);
begin
  self.Close;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.OnScanSelected(Sender: TObject);
begin
  self.ScanCurFolder;
end;
//------------------------------------------------------------------------------
//
//                                     RESIZING
//
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.FormResize(Sender: TObject);
begin
  if FRunning then
    begin
      Log('Resize');

      BtnExit.Left   := BRD;
      BtnExit.Width  := BTNWDT;
      BtnExit.Top    := self.ClientHeight - BTNHGT - BRD;
      BtnExit.Height := BTNHGT;

      BtnScan.Left   := BTNWDT + BRD * 2;
      BtnScan.Width  := BTNWDT;
      BtnScan.Top    := BtnExit.Top;
      BtnScan.Height := BTNHGT;

      BtnClear.Left   := BTNWDT * 2 + BRD * 3;
      BtnClear.Width  := BTNWDT;
      BtnClear.Top    := BtnExit.Top;
      BtnClear.Height := BTNHGT;

      BtnClean.Left   := BTNWDT * 3 + BRD * 4;
      BtnClean.Width  := BTNWDT;
      BtnClean.Top    := BtnExit.Top;
      BtnClean.Height := BTNHGT;

      CountLab.Left   := BTNWDT * 4 + BRD * 6;
      CountLab.Top    := BtnExit.Top + ((BTNHGT - CountLab.Height) div 2);

      Panel.Left   := BRD;
      Panel.Width  := self.ClientWidth - BRD * 2;
      Panel.Top    := BRD;
      Panel.Height := BtnExit.Top - BRD * 2;

      UiRefresh;
    end;
end; 
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.Splitter1Moved(Sender: TObject);
begin
  Log('Splitter1Moved');
  // Folder List has Changed Height

  FolderListView.Columns[0].Width := FILEWDT;
  FolderListView.Columns[1].Width := FolderListView.ClientWidth - FILEWDT;
  FileListView.Columns[0].Width := FileListView.ClientWidth;

  if FRunning then
    FSlitterA.pInt := FolderListView.Height;
end;
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------
procedure TMainForm.Splitter2Moved(Sender: TObject);
begin
  Log('Splitter2Moved');
  // File List has Changed Height

  FolderListView.Columns[0].Width := FILEWDT;
  FolderListView.Columns[1].Width := FolderListView.ClientWidth - FILEWDT;
  FileListView.Columns[0].Width := FileListView.ClientWidth;
  
  if FRunning then
    FSlitterB.pInt := FileListView.Height;
end;
//------------------------------------------------------------------------------
//
//                                     GENERIC
//
//------------------------------------------------------------------------------
// Log
//------------------------------------------------------------------------------
procedure TMainForm.Log(sLine: String);
begin
  if Assigned(PmaLog) then
    PmaLog.Log(sLine);
end;
//------------------------------------------------------------------------------
// Log to LogWin
//------------------------------------------------------------------------------
procedure TMainForm.LogToWin(sLine: String);
begin
  LogWin.Lines.Append(sLIne);
end;


end.
